<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;			

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/install', 'InstallController@install');
Route::post('/registerClient', 'InstallController@registerClient');

Route::get('/', function () {
        $query = http_build_query([
            'client_id' => config('school.client_id'), // Replace with Client ID
            'redirect_uri' => config('app.url') . '/callback',
            'response_type' => 'code',
            'scope' => ''
        ]);

        return redirect(config('school.platform_url') . '/oauth/authorize?'.$query);
    });

    Route::get('/callback', function (Request $request) {
        $response = (new GuzzleHttp\Client)->post(config('school.platform_url') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => config('school.client_id'),
                'client_secret' => config('school.client_secret'),
                'redirect_uri' => config('app.url') . '/callback',
                'code' => $request->code,
            ]
        ]);

        session()->put('token', json_decode((string) $response->getBody(), true));

        return redirect('/get_user');
    });

    Route::get('/get_user', function () {
        $response = (new GuzzleHttp\Client)->get(config('school.platform_url') . '/api/auth/user', [
            'headers' => [
                'Authorization' => 'Bearer '.session()->get('token.access_token')
            ]
        ]);

        return json_decode((string) $response->getBody(), true);
    });