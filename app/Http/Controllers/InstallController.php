<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InstallController extends Controller
{
    
    private function setLaravelEnv($key, $value) {
        $envPath = app()->environmentFilePath();
        $oldFileContents = file_get_contents($envPath);
        $newContent = $key . '=' . $value;

        $idx = strpos($oldFileContents, $key);
        $left = substr($oldFileContents, 0, $idx);
        $prevRight = substr($oldFileContents, $idx);
        $right = substr($prevRight, strpos($prevRight, chr(10)));
        $newFileContents = $left . $newContent . $right;

        file_put_contents($envPath, $newFileContents);
    }

    public function install() {
        return view('install.web');
    }

    public function registerClient(Request $request) {
        $installMode = $request->input('install-type');
        $schoolName = $request->input('school-name');
        $domainName = $request->input('domain-name');

        // register client
        if ($installMode == 'platform') {
            $response = (new \GuzzleHttp\Client)->post('165.22.246.95/api/auth/registerClient', [
                'form_params' => [
                    'name' => $schoolName,
                    'redirect' => join('/', array(trim($domainName . '/callback', '/')))
                ]
            ]);

            // set environment variable(s)
            $client = json_decode($response->getBody(), false);
            $this->setLaravelEnv('SS_PLATFORM_CLIENT_ID', $client->id);
            $this->setLaravelEnv('SS_PLATFORM_SECRET', $client->secret);
        }

        $this->setLaravelEnv('APP_URL', $domainName);

        return redirect('/');
    }
}
