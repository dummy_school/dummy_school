@extends('template')

@section('content')
    <h1>Install Dummy School</h1>
    <form method="POST" action="/registerClient">
        @csrf
        <fieldset>
            <legend>Install Mode</legend>
            <input type="radio" name="install-type" value="platform" />
            <label for="platform">Connected to Platform</label>
            <br />
            <input type="radio" name="install-type" value="standalone" />
            <label for="platform">Standalone</label>
        </fieldset>

        <fieldset>
            <legend>School Configurations</legend>
            <label for="school-name">School Name</label>
            <input type="text" name="school-name" size="75" />
            <br />
            <label for="domain-name">Domain Name</label>
            <input type="text" name="domain-name"  size="75" />
            <br />
        </fieldset>
        <button class="btn btn-primary">Install</button>
    </form>
@endsection