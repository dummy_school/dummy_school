<?php 
    return [
        'platform_url' => env('SS_PLATFORM_URL', 'http://localhost'),
        'client_id' => env('SS_PLATFORM_CLIENT_ID', 1),
        'client_secret' => env('SS_PLATFORM_SECRET', ''),
    ];
